import {useState} from "react";
import {IpDataState} from "../interfaces/homePage.ts";

export const useIPForm = () => {
    const [IpData, setIpData] = useState<IpDataState>({
        status: 'IP',
        ip: '',
        ipRangoDesde: '',
        ipRangoHasta: '',
        ipDescripcion: '',
        file: null,
    });

    const [validationErrors, setValidationErrors] = useState<{ [key: string]: string }>({
        ip: '',
        ipRangoDesde: '',
        ipRangoHasta: '',
    });


    const resetForm = () => {
        setIpData({
            status: 'IP',
            ip: '',
            ipRangoDesde: '',
            ipRangoHasta: '',
            ipDescripcion: '',
            file: null,
        });
        setValidationErrors({
            ip: '',
            ipRangoDesde: '',
            ipRangoHasta: '',
        });
    };
    const [status, setStatus] = useState('IP');


    return { ...IpData, validationErrors, resetForm, setIpData, setValidationErrors, status, setStatus };
};
