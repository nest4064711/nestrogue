export interface Vulnerabilidad {
  id: number,
  vulnerabilidad: string, 
  resumen: string, 
  descripcion: string, 
  riesgo: string, 
  cve: string
}