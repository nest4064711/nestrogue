export interface RangosIPInterface {
  ip_desde: string;
  ip_hasta: string;
  descripcion: string;
  criticidades: {
    criticas: number;
    advertencias: number;
    informacion: number;
  };
}
