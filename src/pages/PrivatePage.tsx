import Header from "../components/Header"


export const PrivatePage = ({children}:{children: JSX.Element | JSX.Element[]}) => {
  return (
    <>
      <Header />
      {children}
    </>
  )
}
