import { Container } from "../components/Container";
import { RangosIPInterface } from "../interfaces/RangosIPInterface";
import { useNavigate } from 'react-router-dom';

export const RangoIP = () => {
  const rangos: RangosIPInterface[] = [
    {
      ip_desde: "192.168.1.1",
      ip_hasta: "192.168.1.255",
      descripcion: "Subred local 1",
      criticidades: {
        criticas: 2, 
        advertencias: 5,
        informacion: 10
      }
    },
    {
      ip_desde: "192.168.2.1",
      ip_hasta: "192.168.2.255",
      descripcion: "Subred local 2",
      criticidades: {
        criticas: 3,
        advertencias: 4,
        informacion: 8
      }
    },
        {
      ip_desde: "192.168.2.1",
      ip_hasta: "192.168.2.255",
      descripcion: "Subred local 2",
      criticidades: {
        criticas: 3,
        advertencias: 4,
        informacion: 8
      }
    }
    
  ];

  const navigate = useNavigate();

  const criticidad = () => {
    navigate('/criticidades');
  };

  return (
    <div className="bg-white">
      <h1 className="text-2xl font-bold text-black mb-4 text-center mt-8">Seleccionar Rango de IP: </h1>

      <div className="justify-center w-[80%] mx-auto pb-5">
        <div className="flex flex-col overflow-y-auto bg-white gap-4">
          {rangos.map((rango, index) => (
              <Container key={index} className="w-full flex md:flex-row gap-4 md:gap-10 items-center justify-between flex-wrap">
                <div className="flex-col gap-4 md:flex-row md:gap-10">
                  <div className="flex flex-col gap-2 md:w-auto" >
                    <h2 className="text-2xl font-bold text-gray-800 md:mb-4">{rango.descripcion}</h2>
                    <div className="flex flex-wrap mb-2">
                      <p className="text-xl text-gray-700 mr-1"><span className="font-bold mr-1">IP Desde:</span>{rango.ip_desde}</p>
                      <p className="text-xl text-gray-700 mr-1"><span className="font-bold  mr-1">Hasta:</span>{rango.ip_hasta}</p>
                    </div>
                  </div>
                  
                  <div className="flex-col ">
                    <span className="text-xl text-error mr-1 "><span className="font-bold mr-1">Críticas:</span>{rango.criticidades.criticas}</span>
                    <span className="text-xl text-warning mr-1"><span className="font-bold mr-1"> Advertencias:</span>{rango.criticidades.advertencias}</span>
                    <span className="text-xl text-info mr-1"><span className="font-bold mr-1"> Información:</span>{rango.criticidades.informacion}</span>
                  </div>
                </div>
                <div className="flex flex-wrap"> 
                  <button onClick={criticidad} className="bg-blue-900 hover:bg-blue-700 text-white font-bold py-3 px-4 rounded">Visualizar IPs</button>
                </div>
              </Container>          
            ))}
        </div>
      </div>

    </div>
  );
}