import {CriticidadType} from "../../components/BotonCriticidad";

export interface DispositivoData {
  ip: string;
  nombre: string;
  tipo: "server" | "server-web" | "server-bd" | "router" | "pc";
  error?: number;
  warning?: number;
  info?: number;
}

const datosError: DispositivoData[] = [
  {ip: '192.168.0.1', nombre: 'Dispositivo 1', tipo: 'server', error: 3},
  {ip: '192.168.0.2', nombre: 'Dispositivo 2', tipo: 'server-web', error: 2},
  {ip: '192.168.0.3', nombre: 'Dispositivo 3', tipo: 'router', error: 1},
  {ip: '192.168.0.1', nombre: 'Dispositivo 4', tipo: 'server', error: 3},
  {ip: '192.168.0.2', nombre: 'Dispositivo 5', tipo: 'server-web', error: 2},
  {ip: '192.168.0.3', nombre: 'Dispositivo 6', tipo: 'router', error: 1},
  {ip: '192.168.0.1', nombre: 'Dispositivo 7', tipo: 'server', error: 3},
  {ip: '192.168.0.2', nombre: 'Dispositivo 8', tipo: 'server-web', error: 2, info: 1},
  {ip: '192.168.0.3', nombre: 'Dispositivo 9', tipo: 'router', error: 1, warning: 1},
  {ip: '192.168.0.1', nombre: 'Dispositivo 10', tipo: 'server', error: 3},
  {ip: '192.168.0.2', nombre: 'Dispositivo 11', tipo: 'server-web', error: 2, info: 1, warning: 1},
  {ip: '192.168.0.3', nombre: 'Dispositivo 12', tipo: 'router', error: 1},
];

const datosWarning = [
  { ip: '192.168.0.1', nombre: 'Dispositivo 1', tipo: 'server', warning: 3 },
  { ip: '192.168.0.2', nombre: 'Dispositivo 2', tipo: 'server-web', warning: 2 },
  { ip: '192.168.0.3', nombre: 'Dispositivo 3', tipo: 'router', warning: 1 },
  { ip: '192.168.0.1', nombre: 'Dispositivo 4', tipo: 'server', warning: 3, error: 1},
  { ip: '192.168.0.2', nombre: 'Dispositivo 5', tipo: 'server-web', warning: 2 },
  { ip: '192.168.0.3', nombre: 'Dispositivo 6', tipo: 'router', warning: 1, info: 1},
  { ip: '192.168.0.1', nombre: 'Dispositivo 7', tipo: 'server', warning: 3 },
  { ip: '192.168.0.2', nombre: 'Dispositivo 8', tipo: 'server-web', warning: 2, info: 1, error: 1},
  { ip: '192.168.0.3', nombre: 'Dispositivo 9', tipo: 'router', warning: 1 },
  { ip: '192.168.0.1', nombre: 'Dispositivo 10', tipo: 'server', warning: 3 },
  { ip: '192.168.0.2', nombre: 'Dispositivo 11', tipo: 'server-web', warning: 2 },
  { ip: '192.168.0.3', nombre: 'Dispositivo 12', tipo: 'router', warning: 1 },
] as DispositivoData[]; // Aserción de tipo aquí

const datosInfo = [
  { ip: '192.168.0.1', nombre: 'Dispositivo 1', tipo: 'server', info: 3},
  { ip: '192.168.0.2', nombre: 'Dispositivo 2', tipo: 'server-web', info: 2},
  { ip: '192.168.0.3', nombre: 'Dispositivo 3', tipo: 'router', info: 1},
  { ip: '192.168.0.1', nombre: 'Dispositivo 4', tipo: 'server', info: 3, error: 1, warning: 1},
  { ip: '192.168.0.2', nombre: 'Dispositivo 5', tipo: 'server-web', info: 2, error: 1 },
  { ip: '192.168.0.3', nombre: 'Dispositivo 6', tipo: 'router', info: 1},
  { ip: '192.168.0.1', nombre: 'Dispositivo 7', tipo: 'server', info: 3 },
  { ip: '192.168.0.2', nombre: 'Dispositivo 8', tipo: 'server-web', info: 3 },
  { ip: '192.168.0.3', nombre: 'Dispositivo 9', tipo: 'router', info: 3 },
  { ip: '192.168.0.1', nombre: 'Dispositivo 10', tipo: 'server', info: 3 },
  { ip: '192.168.0.2', nombre: 'Dispositivo 11', tipo: 'server-web', info: 3 },
  { ip: '192.168.0.3', nombre: 'Dispositivo 12', tipo: 'router', info: 3 },
] as DispositivoData[]; // Aserción de tipo aquí


export const ObtenerDispositivosService = (filtro: CriticidadType): DispositivoData[] => {

  switch (filtro) {
    case 'error':
      return datosError;
    case 'warning':
      return datosWarning;
    case 'info':
      return datosInfo;
    default:
      return [];
  }
}