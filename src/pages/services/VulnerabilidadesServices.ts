import { Vulnerabilidad } from "../../interfaces/Vulnerabilidad";

const vulnerabilidades: Vulnerabilidad[] = [
    {
        id: 1,
        vulnerabilidad: "GNU Bash Environment Variable Handling Code Injection (Shellshock)",
        resumen: "El servidor web remoto está afectado por una vulnerabilidad que permite la ejecución remota de código.",
        descripcion: "La vulnerabilidad se debe al procesamiento de comandos finales después de la definición de funciones en los valores de las variables de entorno. Esto permite a un atacante remoto ejecutar código arbitrario a través de la manipulación de variables de entorno dependiendo de la configuración del sistema.",
        riesgo: "Crítico",
        cve: "CVE-2014-6271"
    },
    {
        id: 2,
        vulnerabilidad: "SQL Injection en módulo de autenticación",
        resumen: "Posibilidad de inyección SQL en el sistema de login.",
        descripcion: "Una vulnerabilidad de inyección SQL permite a atacantes ejecutar consultas SQL arbitrarias, lo que puede llevar a la toma de control de la base de datos, acceso a datos confidenciales y potencialmente, toma de control del sistema host.",
        riesgo: "Alto",
        cve: "CVE-2018-1001"
    },
    {
        id: 3,
        vulnerabilidad: "Cross-Site Scripting (XSS) en página de contacto",
        resumen: "La página de contacto es vulnerable a XSS.",
        descripcion: "La vulnerabilidad de tipo Cross-Site Scripting permite a atacantes inyectar scripts del lado del cliente en páginas web vistas por otros usuarios. Esto puede ser usado para robar información de sesión, atacar al navegador y redirigir al usuario a sitios maliciosos.",
        riesgo: "Medio",
        cve: "CVE-2019-2002"
    }
];

export const obtenerVulnerabilidades = (idDispositivo: string) => {
    if (idDispositivo) {
        return vulnerabilidades;
    }
}