import { useEffect, useState } from "react"
import { BotonCriticidad, CriticidadType } from "../components/BotonCriticidad"
import { Container } from "../components/Container"
import { DispositivosEncontrados } from "../components/DispositivosEncontrados.tsx";
import { ListaDispositivos } from "../components/ListaDispositivos.tsx";
import { ListaDispositivosLista } from "../components/ListaDispositivosLista.tsx";
import { DispositivoData, ObtenerDispositivosService } from "./services/CriticidadesServices.ts";
import { ListaVulnerabilidades } from "../components/ListaVulnerabilidades.tsx";

export const Criticidades = () => {

    const [botonSeleccionado, setBotonSeleccionado] = useState<CriticidadType | undefined>(undefined)
    const [dispositivoSeleccionado, setDispositivoSeleccionado] = useState<DispositivoData | undefined>(undefined)
    const [dispositivos, setDispositivos] = useState<DispositivoData[]>([])
    const [modoVisualizacion, setModoVisualizacion] = useState("grilla")

    useEffect(() => {
        if (botonSeleccionado) {
            const data = ObtenerDispositivosService(botonSeleccionado)
            setDispositivos(data)
        }
    }, [botonSeleccionado])


    return (
        <div className="px-10 py-5">
            <DispositivosEncontrados />
            <Container className="flex flex-col md:flex-row gap-4 md:gap-10 px-4 md:px-10">
                <BotonCriticidad tipo="error" cantidadRiesgos={12}
                    cantDispositivos={6} activo={botonSeleccionado === "error"} onClick={setBotonSeleccionado} />

                <BotonCriticidad tipo="warning" cantidadRiesgos={4}
                    cantDispositivos={1} activo={botonSeleccionado === "warning"} onClick={setBotonSeleccionado} />

                <BotonCriticidad tipo="info" cantidadRiesgos={7}
                    cantDispositivos={3} activo={botonSeleccionado === "info"} onClick={setBotonSeleccionado} />

            </Container>
            {
                botonSeleccionado &&
                <Container className="mt-5">
                    <div className="flex justify-end items-center gap-2">
                        <button
                            onClick={() => setModoVisualizacion('grilla')}
                            aria-label="Cambiar a vista de grilla"
                            style={{fill: modoVisualizacion === 'grilla' ? '#4D4D4D' : '#bdbdbd'}}>
                            <svg xmlns="http://www.w3.org/2000/svg"
                                 width="16"
                                 viewBox="0 0 512 512">
                                <path d="M64 32C28.7 32 0 60.7 0 96V416c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V96c0-35.3-28.7-64-64-64H64zm88 64v64H64V96h88zm56 0h88v64H208V96zm240 0v64H360V96h88zM64 224h88v64H64V224zm232 0v64H208V224h88zm64 0h88v64H360V224zM152 352v64H64V352h88zm56 0h88v64H208V352zm240 0v64H360V352h88z"/>
                            </svg>
                        </button>
                        <button
                            onClick={() => setModoVisualizacion('lista')}
                            aria-label="Cambiar a vista de lista"
                            style={{fill: modoVisualizacion === 'lista' ? '#4D4D4D' : '#bdbdbd'}}>
                            <svg xmlns="http://www.w3.org/2000/svg"
                                 width="16"
                                 viewBox="0 0 512 512">
                                <path d="M40 48C26.7 48 16 58.7 16 72v48c0 13.3 10.7 24 24 24H88c13.3 0 24-10.7 24-24V72c0-13.3-10.7-24-24-24H40zM192 64c-17.7 0-32 14.3-32 32s14.3 32 32 32H480c17.7 0 32-14.3 32-32s-14.3-32-32-32H192zm0 160c-17.7 0-32 14.3-32 32s14.3 32 32 32H480c17.7 0 32-14.3 32-32s-14.3-32-32-32H192zm0 160c-17.7 0-32 14.3-32 32s14.3 32 32 32H480c17.7 0 32-14.3 32-32s-14.3-32-32-32H192zM16 232v48c0 13.3 10.7 24 24 24H88c13.3 0 24-10.7 24-24V232c0-13.3-10.7-24-24-24H40c-13.3 0-24 10.7-24 24zM40 368c-13.3 0-24 10.7-24 24v48c0 13.3 10.7 24 24 24H88c13.3 0 24-10.7 24-24V392c0-13.3-10.7-24-24-24H40z"/>
                            </svg>
                        </button>
                    </div>
                    {modoVisualizacion === 'grilla' ? (
                        <ListaDispositivos botonSeleccionado={botonSeleccionado} lista={dispositivos}
                           idDispositivoSeleccionado={dispositivoSeleccionado?.ip}
                           onSelectDispositivo={setDispositivoSeleccionado}
                        />
                    ) : (
                        <ListaDispositivosLista botonSeleccionado={botonSeleccionado} lista={dispositivos}
                            idDispositivoSeleccionado={dispositivoSeleccionado?.ip}
                            onSelectDispositivo={setDispositivoSeleccionado}
                        />
                    )}
                </Container>
            }
            {
                dispositivoSeleccionado &&
                <ListaVulnerabilidades dispositivo={dispositivoSeleccionado} onCloseModal={() => setDispositivoSeleccionado(undefined)} />
            }
        </div>
    )
}