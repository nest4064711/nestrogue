import React, { useCallback, useEffect, useState } from "react";
import { Input } from "../components/Input.tsx";
import { useIPForm } from "../hooks/useIPForm.ts";
import { useNavigate } from "react-router-dom";
import { Button } from "../components/Button.tsx";
import { IpDataState } from "../interfaces/homePage.ts";
import ConfirmDialog from "../components/DialogoConfirmacion.tsx"; // Asegúrate de importar el componente correctamente

export const HomePage = () => {
    const { ip, status, file, validationErrors, resetForm, setIpData, setValidationErrors, setStatus } = useIPForm(); // Asegúrate de desestructurar setStatus
    const [ipRanges, setIpRanges] = useState<IpDataState[]>([]);
    const navigate = useNavigate();
    const [showConfirmDialog, setShowConfirmDialog] = useState(false);
    const [newStatus, setNewStatus] = useState(status);

    const handleStatusChange = useCallback((newStatus: string) => {
        const hasData = status === 'IP' ? !!ip : ipRanges.some(range => !!range.ipRangoDesde || !!range.ipRangoHasta);

        if (hasData) {
            setNewStatus(newStatus);
            setShowConfirmDialog(true);
        } else {
            setStatus(newStatus);
        }
    }, [status, ip, ipRanges]);



    const handleConfirmChange = () => {
        setStatus(newStatus);
        setShowConfirmDialog(false);
    };

    const handleCancelChange = () => {
        setShowConfirmDialog(false);
    };

    const validateIP = (ip: string): boolean => {
        const pattern = /^(\d{1,3}\.){3}\d{1,3}(\/\d{1,2})?$/;
        return pattern.test(ip);
    };

    useEffect(() => {
        setIpData(prevState => ({ ...prevState, ip: '' }));
        setValidationErrors(prevState => ({ ...prevState, ip: '' }));
        setIpRanges([]);
    }, [status]);

    const handleInputChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
        const { id, value } = e.target;
        if (id.includes('ipRangoDesde') || id.includes('ipRangoHasta') || id.includes('ipDescripcion')) {
            const index = parseInt(id.replace(/\D/g,''));
            setIpRanges(prevState => prevState.map((ipRange, i) => i === index ? { ...ipRange, [id.replace(/[0-9]/g, '')]: value } : ipRange));
        } else if (id === 'status') {
            handleStatusChange(value);
        } else {
            setIpData(prevState => ({ ...prevState, [id]: value }));
        }

        if (id.includes('ip') || id.includes('ipRangoDesde') || id.includes('ipRangoHasta')) {
            setValidationErrors(prevState => ({
                ...prevState,
                [id]: validateIP(value) ? '' : 'Por favor, introduce una dirección IP válida.'
            }));
        }
    }, [setIpData, setValidationErrors, handleStatusChange]); // Asegúrate de incluir handleStatusChange en la lista de dependencias




    const handleFileChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files?.[0];
        if (!file) {
            console.error("No se seleccionó ningún archivo.");
            return;
        }

        if (file.type !== 'text/csv') {
            console.error("El archivo seleccionado no es un archivo CSV.");
            return;
        }

        const reader = new FileReader();
        reader.onload = (e) => {
            const text = e.target?.result as string;
            if (!text) {
                console.error("El archivo CSV está vacío.");
                return;
            }

            const lines = text.split('\n');
            const newIpRanges = lines.map(line => {
                // aquí se divide la línea en partes usando la coma como delimitador
                const parts = line.split(',');
                const ipRangoDesde = parts[0]?.trim() || '';
                const ipRangoHasta = parts[1]?.trim() || '';
                const ipDescripcion = parts[2]?.trim() || '';

                return {
                    status: 'RangeIP',
                    ip: '',
                    ipRangoDesde,
                    ipRangoHasta,
                    ipDescripcion,
                    file: null,
                } as IpDataState;
            });
            setIpRanges(newIpRanges);
        };
        reader.onerror = (error) => {
            console.error("Error al leer el archivo:", error);
        };
        reader.readAsText(file);
    }, []);




    const handleDrop = useCallback((e: React.DragEvent<HTMLDivElement>) => {
        e.preventDefault();
        if (e.dataTransfer.items) {
            for (let i = 0; i < e.dataTransfer.items.length; i++) {
                if (e.dataTransfer.items[i].kind === 'file') {
                    const file = e.dataTransfer.items[i].getAsFile();
                    if (file && file.type === 'text/csv') {
                        handleFileChange({ target: { files: [file] } } as never);
                    } else {
                        alert('Por favor, selecciona un archivo .csv.');
                    }
                }
            }
        }
    }, [handleFileChange]);

    const handleDragOver = useCallback((e: React.DragEvent<HTMLDivElement>) => {
        e.preventDefault();
    }, []);

    const addIpRange = () => {
        setIpRanges([...ipRanges, {
            status: 'RangeIP',
            ip: '',
            ipRangoDesde: '',
            ipRangoHasta: '',
            ipDescripcion: '',
            file: null,
        }]);
    };

    const removeIpRange = (index: number) => {
        setIpRanges(prevState => prevState.filter((_, i) => i !== index));
    };

    const goToCriticidades = () => {
        if (status === 'RangeIP') {
            navigate('/rangos-ip');
        } else if (status === 'IP') {
            navigate('/criticidades');
        }
    }

    return (
        <div className="flex justify-center items-center bg-black" style={{ height: 'calc(100vh - 72px)' }}>
            <div className="w-full lg:max-w-screen-lg md:max-w-screen-md max-h-[95%] sm:max-w-screen-sm max-w-screen max-w-[500px] p-5 bg-gray-50 shadow-md rounded-md mx-9 overflow-y-auto">
                <fieldset className="border p-4 rounded-md">
                    <legend className="text-lg font-medium text-gray-700">Configuración de IP</legend>

                    <div className="flex items-center space-x-4 my-2">
                        <input id="status" className="form-radio" type="radio" name="status" value="IP"
                               checked={status === 'IP'} onChange={handleInputChange}/>
                        <label htmlFor="status" className="text-gray-700">IP</label>
                    </div>

                    <div className="flex items-center space-x-4 my-2">
                        <input id="status" className="form-radio text-blue-500" type="radio" name="status"
                               value="RangeIP"
                               checked={status === 'RangeIP'} onChange={handleInputChange}/>
                        <label htmlFor="status" className="text-gray-700">Rango de IP</label>
                    </div>

                    {status === 'IP' && (
                        <Input
                            id="ip"
                            label="IP"
                            placeholder="xxx.xxx.xxx/nn"
                            value={ip}
                            onChange={handleInputChange}
                            errorMessage={validationErrors.ip}
                        />
                    )}
                    {status === 'RangeIP' && (
                        <div className="divide-y divide-gray-200">
                            {ipRanges.map((ipRange, index) => (
                                <div className="flex flex-col sm:flex-row space-y-2 sm:space-y-0 space-x-2 mt-1" key={index}>
                                    <div className="flex flex-col sm:flex-col md:flex-row space-y-2 sm:space-y-0 md:space-x-2 w-full mb-2">
                                        <div className="w-full sm:w-11/12">
                                            <Input
                                                id={`ipRangoDesde${index}`}
                                                label="IP Desde"
                                                placeholder="xxx.xxx.xxx/nn"
                                                value={ipRange.ipRangoDesde}
                                                onChange={handleInputChange}
                                                errorMessage={validationErrors[`ipRangoDesde${index}`]}
                                            />
                                        </div>
                                        <div className="w-full sm:w-11/12">
                                            <Input
                                                id={`ipRangoHasta${index}`}
                                                label="IP Hasta"
                                                placeholder="xxx.xxx.xxx/nn"
                                                value={ipRange.ipRangoHasta}
                                                onChange={handleInputChange}
                                                errorMessage={validationErrors[`ipRangoHasta${index}`]}
                                            />
                                        </div>
                                        <div className="w-full sm:w-11/12">
                                            <Input
                                                id={`ipDescripcion${index}`}
                                                label="Descripción"
                                                placeholder="Ej: Servidor Nest"
                                                value={ipRange.ipDescripcion}
                                                onChange={handleInputChange}
                                            />
                                        </div>
                                    </div>
                                    <div className="flex items-center justify-center">
                                        <Button name={"Eliminar"} color={"red"} className={"mt-0 mb-2 sm:mt-5 sm:mb-0"} onClick={() => removeIpRange(index)}/>
                                    </div>
                                </div>
                            ))}
                        </div>
                    )}

                    {status === 'RangeIP' && (
                        <Button name={"Agregar Rango"} color={"green"} className={"mb-4 mt-2"} onClick={addIpRange}/>
                    )}
                    {status === 'RangeIP' && (
                        <div
                            onDrop={handleDrop}
                            onDragOver={handleDragOver}
                            className="mt-1 flex w-full h-40 border-dashed border-2 content-center items-center justify-center rounded-md"
                        >

                            <input
                                type="file"
                                id="file"
                                onChange={handleFileChange}
                                accept=".csv"
                                className="hidden"
                            />
                            <label htmlFor="file" className="cursor-pointer">
                                {file ? file.name : 'Seleccionar archivo o arrastrar y soltar'}
                            </label>
                        </div>

                    )}
                    <Button
                        onClick={goToCriticidades}
                        color="orange"
                        className="mt-4 font-bold py-2 px-4 rounded"
                        name="Seleccionar IP"
                        disabled={
                            status === 'IP' ? !!validationErrors.ip || !ip :
                                ipRanges.some((range, index) =>
                                    !!validationErrors[`ipRangoDesde${index}`] ||
                                    !!validationErrors[`ipRangoHasta${index}`] ||
                                    !range.ipRangoDesde ||
                                    !range.ipRangoHasta
                                )
                        }
                    />
                    {status === 'IP' && (
                    <Button onClick={resetForm} color="gray" className="mt-4 font-bold py-2 px-4 ml-2 rounded"
                            name="Limpiar"/>
                    )}
                </fieldset>
            </div>
            <ConfirmDialog
                isOpen={showConfirmDialog}
                onConfirm={handleConfirmChange}
                onCancel={handleCancelChange}
                title="Confirmar cambio"
                message="Cambiarás de IP a Rango de IP. Los datos actuales se perderán. ¿Deseas continuar?"
            />
        </div>
    );
};
