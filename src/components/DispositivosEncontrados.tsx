
// Objeto de ejemplo
const dispositivo = {
    descripcion: 'Servidores de Nest',
    rango: true,
    ip: '192.168.1.1',
    ip2: '192.168.1.255',
    ips: ['192.168.1.10', '192.168.1.20', '192.168.1.30'],
};

export const DispositivosEncontrados = () => {
    let subtitulo = '';

    if (dispositivo.rango) {
        subtitulo = `Desde ${dispositivo.ip} Hasta ${dispositivo.ip2}`;
    } else if (dispositivo.ip) {
        subtitulo = `IP ${dispositivo.ip}`;
    } else if (dispositivo.ips && dispositivo.ips.length > 0) {
        subtitulo = `IPs: ${dispositivo.ips.join(', ')}`;
    }

    return (
        <div className="flex flex-col">
            <h1 className="text-2xl sm:text-3xl font-bold text-gray-700 mb-4">{dispositivo.descripcion}</h1>
            <h1 className="text-lg sm:text-2xl font-bold text-gray-700 mb-4">Dispositivos encontrados: <span className="text-lg sm:text-xl text-orange-400 text-center">{subtitulo}</span></h1>
        </div>
    );
};
