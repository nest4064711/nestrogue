import {CriticidadType} from "./BotonCriticidad.tsx";
import {DispositivoData} from "../pages/services/CriticidadesServices.ts";
import React, {useEffect, useState} from "react";
import {Button} from "./Button.tsx";

interface Props {
    botonSeleccionado: CriticidadType;
    lista: DispositivoData[];
    idDispositivoSeleccionado?: string;
    onSelectDispositivo?: (dispositivo: DispositivoData | undefined) => void;
}

export const ListaDispositivosLista: React.FC<Props> = ({
    botonSeleccionado,
    lista,
    idDispositivoSeleccionado,
    onSelectDispositivo = () => {}
}) => {
    const [paginaActual, setPaginaActual] = useState(1);
    const [datosPaginados, setDatosPaginados] = useState<DispositivoData[]>([]);
    const [totalPaginas, setTotalPaginas] = useState(0);
    const elementosPorPagina = 100;

    const colors = {
        'error': '#DC3545',
        'warning': '#FFC107',
        'info': '#28A745'
    }

    useEffect(() => {
        setPaginaActual(1);
        onSelectDispositivo(undefined);
    }, [botonSeleccionado, onSelectDispositivo]);

    useEffect(() => {
        const datos = lista.slice((paginaActual - 1) * elementosPorPagina, paginaActual * elementosPorPagina);
        setDatosPaginados(datos);
    }, [lista, paginaActual]);

    useEffect(() => {
        const totalPags = Math.ceil(lista.length / elementosPorPagina);
        setTotalPaginas(totalPags);
    }, [lista]);

    const paginaAnterior = () => {
        if (paginaActual > 1) {
            setPaginaActual(paginaActual - 1);
        }
    };

    const paginaSiguiente = () => {
        if (paginaActual < totalPaginas) {
            setPaginaActual(paginaActual + 1);
        }
    };

    const onDispositivoClicked = (dispositivo: DispositivoData) => {
        if(dispositivo.ip !== idDispositivoSeleccionado)
            onSelectDispositivo(dispositivo)
        else
            onSelectDispositivo(undefined)
    }

    return (
        <div className="flex flex-col mt-5">
            {datosPaginados.map((dispositivo) => (
                <div key={dispositivo.ip} onClick={() => onDispositivoClicked(dispositivo)} className="w-full bg-gray-100 mx-auto border-gray-500 border rounded-sm text-gray-700 mb-0.5 h-30 cursor-pointer">
                    <div className="flex flex-col sm:flex-row md:flex-row lg:flex-row xl:flex-row p-3 border-l-8 w-full justify-evenly items-center"
                         style={{borderColor: `${colors[botonSeleccionado]}`}}>
                        <div className="flex text-center md:items-center lg:items-center justify-evenly w-full sm:w-6/12 md:w-6/12 lg:w-6/12 flex-wrap sm:flex-nowrap">
                            <div className="text-xs sm:text-sm md:text-md lg:text-lg leading-5 font-semibold">
                                <span className="text-sm lg:text-sm leading-4 font-normal text-gray-500"> Tipo:</span> {dispositivo.tipo}
                            </div>
                            <div className="text-xs sm:text-sm md:text-md lg:text-lg leading-5 font-semibold mx-4 lg:mx-2"
                                style={{
                                    maxWidth: '200px',
                                    whiteSpace: 'normal',
                                }}>
                                <span className="text-sm lg:text-sm leading-4 font-normal text-gray-500"> Nombre:</span> {dispositivo.nombre}
                            </div>
                            <div className="text-xs sm:text-sm md:text-md lg:text-lg leading-5 font-semibold">
                                <span className="text-sm lg:text-sm leading-4 font-normal text-gray-500"> IP:</span> {dispositivo.ip}
                            </div>
                        </div>
                        <div className="flex items-center justify-evenly w-full sm:w-6/12 md:w-6/12 lg:w-6/12 flex-wrap sm:flex-nowrap sm:ml-1.5">
                            <div className="bg-[#DC3545] text-xs md:text-sm lg:text-lg px-1 sm:px-1.5 md:px-2 lg:px-2.5 py-0.5 rounded-md leading-4 font-semibold text-center text-white">
                                Errores: {dispositivo.error || 0}
                            </div>
                            <div className="bg-[#FFC107] text-xs md:text-sm lg:text-lg px-1 sm:px-1.5 md:px-2 lg:px-2.5 py-0.5 rounded-md my-2 mx-1 leading-4 font-semibold text-center text-white">
                                Advertencias: {dispositivo.warning || 0}
                            </div>
                            <div className="bg-[#28A745] text-xs md:text-sm lg:text-lg px-1 sm:px-1.5 md:px-2 lg:px-2.5 py-0.5 rounded-md leading-4 font-semibold text-center text-white">
                                Informativos: {dispositivo.info || 0}
                            </div>
                        </div>
                    </div>
                </div>
            ))}
            <div className="flex flex-col sm:flex-row justify-end mt-4">
                <Button onClick={paginaAnterior} disabled={paginaActual === 1} name="Anterior"
                        className="mb-2 sm:mb-0 sm:mr-2">Anterior</Button>
                <span className="mx-2 mb-2 sm:mb-0 text-center">Pág. {paginaActual} de {totalPaginas}</span>
                <Button onClick={paginaSiguiente} disabled={paginaActual === totalPaginas} name="Siguiente"
                        className="mb-2 sm:mb-0 sm:ml-2">Siguiente</Button>
            </div>
        </div>
    )
}