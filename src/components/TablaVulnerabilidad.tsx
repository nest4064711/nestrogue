import { Vulnerabilidad } from '../interfaces/Vulnerabilidad';

interface Props extends Vulnerabilidad {

}
export const TablaVulnerabilidad = ({ vulnerabilidad: vuln, resumen, descripcion, riesgo, cve }: Props) => {

  return (
    <div className="flex justify-center">
      <table className="table-auto w-full border-collapse border border-gray-800">

        <tbody>
          <tr>
            <td className="border px-4 py-2">Vulnerabilidad</td>
            <td className="border px-4 py-2">{vuln}</td>
          </tr>
          <tr>
            <td className="border px-4 py-2">Resumen</td>
            <td className="border px-4 py-2">{resumen}</td>
          </tr>
          <tr>
            <td className="border px-4 py-2">Descripción</td>
            <td className="border px-4 py-2">{descripcion}</td>
          </tr>
          <tr>
            <td className="border px-4 py-2">Riesgo</td>
            <td className="border px-4 py-2">{riesgo}</td>
          </tr>
          <tr>
            <td className="border px-4 py-2">CVE</td>
            <td className="border px-4 py-2">{cve}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

// export default TablaVulnerabilidad;
