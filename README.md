# Nest Rogue

Este es un proyecto de monitoreo de IPs para la empresa Nest.


### Tecnologías

Está desarrollado con [React](https://react.dev/), usando [Vite](https://vitejs.dev/). El lenguaje de programación es [TypeScript](https://www.typescriptlang.org/) y los estilos se manejan con [Tailwind CSS](https://tailwindcss.com/).

Versiones de Node aceptadas: ^18.0.0 || >=20.0.0
Versión utilizada: 20 (LTS 04/2024)

| Tecnología   | Versión |
|--------------|---------|
| React        | ^18.2.0 |
| Vite         |  ^5.1.6 |
| TypeScript   |  ^5.2.2 |
| Tailwind CSS |  ^3.4.1 |

_La lista completa de librerías, con sus versiones respectivas, se encuentran en el archivo_ `package.json`

## Instalación y ejecución

Clonar con HTTPS
```bash
git clone https://gitlab.com/nest4064711/nestrogue.git
```

Clonar con SSH
```bash
git clone git@gitlab.com:nest4064711/nestrogue.git
```

Una vez clonado, debemos entrar a la carpeta de nuestro proyecto:
```bash
cd nestrogue
```

Para instalar las librerías necesarias para ejecutar el proyecto:
```bash
npm install
```
_Este comando solo requiere ser ejecutado al abrir el proyecto por primera vez, o cuando se agreguen nuevas librerías._

&nbsp;
&nbsp;

Una vez instalado todo, podemos ejecutar el proyecto:
```bash
npm run dev
```
_Este comando lo ejecutaremos cada vez que querramos ejecutar localmente nuestro proyecto._

&nbsp;

Para generar los archivos de producción, se utiliza el comando:
```bash
npm run build
```
La compilación se guardará en la carpeta `dist/`


&nbsp;

## Estructura de archivos

En la carpeta raíz encontramos los archivos de configuración, así como las carpetas:
* `src` para los archivos del proyecto (lógica de negocios).
* `assets` para los recursos del proyecto.

También encontraremos el archivo `index.html`

&nbsp;

Dentro de `src` tenemos (además de los archivos "padre" del proyecto, que no están en carpetas):

| Carpeta      | Descripción                                                                                                                                                                                                                                                                                            |
|--------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `pages`      | Aquí vemos los archivos correspondientes a las pantallas.<br>Pantalla = Vista + Lógica<br>Las pantallas pueden estar organizadas por carpetas, así como tener una carpeta extra de `services`, para definir las llamadas a servicios web o de obtención de datos.                                  |
| `components` | Aquí vemos los archivos de los componentes utilizados en el proyecto.<br>Un componente es una pieza de código reutilizable, que compone a un elemento visual.<br>Componente = Vista + Lógica                                                                                                           |
| `interfaces` | Aquí se definen los tipos (parecido al modelo) de nuestro proyecto.                                                                                                                                                                                                                                    |
| `router`     | Aquí están definidas las rutas de nuestra aplicación.                                                                                                                                                                                                                                                  |
| `hooks`      | Aquí van nuestros Hooks personalizados.<br>Un hook es una pieza de código reutilizable, donde encontramos lógica de negocios.<br>No contiene elementos visuales, solamente lógica; muchas veces se saca la lógica de una pantalla/componente y se guarda en un hook (separando la lógica de la vista). |


&nbsp;
\
&nbsp;

### React + TypeScript + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

#### Expanding the ESLint configuration

If you are developing a production application, we recommend updating the configuration to enable type aware lint rules:

- Configure the top-level `parserOptions` property like this:

```js
export default {
  // other rules...
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: ['./tsconfig.json', './tsconfig.node.json'],
    tsconfigRootDir: __dirname,
  },
}
```

- Replace `plugin:@typescript-eslint/recommended` to `plugin:@typescript-eslint/recommended-type-checked` or `plugin:@typescript-eslint/strict-type-checked`
- Optionally add `plugin:@typescript-eslint/stylistic-type-checked`
- Install [eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react) and add `plugin:react/recommended` & `plugin:react/jsx-runtime` to the `extends` list
